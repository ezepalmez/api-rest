const express = require("express");
const morgan = require("morgan");
const app = express();

//setting
app.set("port", process.env.PORT || 3000);

// routers
app.use("/api", require("./routes"));

//middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//starting server
app.listen(app.get("port"), () => {
  console.log(`Server on port ${app.get("port")}`);
});
